# Occitan translation for gnome-online-accounts.
# Copyright (C) 2011-2012 gnome-online-accounts's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-online-accounts package.
# Cédric Valmary (Tot en Òc) <cvalmary@yahoo.fr>, 2015.
# Cédric Valmary (totenoc.eu) <cvalmary@yahoo.fr>, 2016, 2018.
msgid ""
msgstr ""
"Project-Id-Version: gnome-online-accounts master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-online-accounts/"
"issues\n"
"POT-Creation-Date: 2021-03-17 09:19+0000\n"
"PO-Revision-Date: 2021-05-10 22:07+0200\n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: Tot En Òc\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 2.4.3\n"
"X-Project-Style: gnome\n"

#: data/org.gnome.online-accounts.gschema.xml:6
msgid "List of providers that are allowed to be loaded"
msgstr "Lista de provesidors autorizats a èsser cargats"

#: data/org.gnome.online-accounts.gschema.xml:7
msgid ""
"A list of strings representing the providers that are allowed to be loaded "
"(default: 'all'). This is only evaluated on startup."
msgstr ""
"Una lista de cadenas representant los provesidors autorizats a èsser cargats "
"(per defaut : « all » (totes)). Aquò es pas evaluat qu’a l'aviada."

#. TODO: more specific
#: src/daemon/goadaemon.c:1143 src/daemon/goadaemon.c:1460
#, c-format
msgid "Failed to find a provider for: %s"
msgstr "Impossible de trobar un provesidor per : %s"

#: src/daemon/goadaemon.c:1403
msgid "IsLocked property is set for account"
msgstr "La proprietat IsLocked es definida per aqueste compte"

#. TODO: more specific
#: src/daemon/goadaemon.c:1448
msgid "ProviderType property is not set for account"
msgstr "La proprietat ProviderType es pas definida per aqueste compte"

#. TODO: more specific
#: src/goabackend/goaewsclient.c:258
msgid "Failed to parse autodiscover response XML"
msgstr "Fracàs de l'analisi XML de la responsa autodiscover"

#. TODO: more specific
#. Translators: the parameter is an XML element name.
#: src/goabackend/goaewsclient.c:269 src/goabackend/goaewsclient.c:284
#: src/goabackend/goaewsclient.c:299
#, c-format
msgid "Failed to find “%s” element"
msgstr "Impossible de trobar l’element « %s »"

#. TODO: more specific
#: src/goabackend/goaewsclient.c:320
msgid "Failed to find ASUrl and OABUrl in autodiscover response"
msgstr "Impossible de trobar ASUrl e OABUrl dins la responsa autodiscover"

#: src/goabackend/goaexchangeprovider.c:51
msgid "Microsoft Exchange"
msgstr "Microsoft Exchange"

#. Translators: the first %s is the username
#. * (eg., debarshi.ray@gmail.com or rishi), and the
#. * (%s, %d) is the error domain and code.
#.
#: src/goabackend/goaexchangeprovider.c:248
#: src/goabackend/goalastfmprovider.c:279
#: src/goabackend/goaowncloudprovider.c:291
#, c-format
msgid "Invalid password with username “%s” (%s, %d): "
msgstr "Senhal invalid per l’utilizaire « %s » (%s, %d) : "

#: src/goabackend/goaexchangeprovider.c:394
#: src/goabackend/goaimapsmtpprovider.c:647
msgid "_E-mail"
msgstr "_Corrièr electronic"

#: src/goabackend/goaexchangeprovider.c:395
#: src/goabackend/goafedoraprovider.c:574
#: src/goabackend/goaimapsmtpprovider.c:667
#: src/goabackend/goaimapsmtpprovider.c:687
#: src/goabackend/goalastfmprovider.c:421
#: src/goabackend/goaowncloudprovider.c:509
msgid "_Password"
msgstr "_Senhal"

#: src/goabackend/goaexchangeprovider.c:398
msgid "_Custom"
msgstr "_Personalizar"

#: src/goabackend/goaexchangeprovider.c:409
#: src/goabackend/goafedoraprovider.c:573
#: src/goabackend/goaimapsmtpprovider.c:666
#: src/goabackend/goaimapsmtpprovider.c:686
#: src/goabackend/goalastfmprovider.c:420
#: src/goabackend/goaowncloudprovider.c:508
msgid "User_name"
msgstr "_Nom d'utilizaire"

#: src/goabackend/goaexchangeprovider.c:410
#: src/goabackend/goaowncloudprovider.c:507
msgid "_Server"
msgstr "_Servidor"

#. --
#: src/goabackend/goaexchangeprovider.c:420
#: src/goabackend/goafedoraprovider.c:581
#: src/goabackend/goaimapsmtpprovider.c:699
#: src/goabackend/goakerberosprovider.c:583
#: src/goabackend/goalastfmprovider.c:429
#: src/goabackend/goaowncloudprovider.c:523
msgid "_Cancel"
msgstr "_Anullar"

#: src/goabackend/goaexchangeprovider.c:421
#: src/goabackend/goafedoraprovider.c:582
#: src/goabackend/goakerberosprovider.c:584
#: src/goabackend/goalastfmprovider.c:430
#: src/goabackend/goaowncloudprovider.c:524
msgid "C_onnect"
msgstr "C_onnexion"

#: src/goabackend/goaexchangeprovider.c:437
#: src/goabackend/goafedoraprovider.c:597
#: src/goabackend/goaimapsmtpprovider.c:715
#: src/goabackend/goakerberosprovider.c:599
#: src/goabackend/goalastfmprovider.c:446
#: src/goabackend/goaowncloudprovider.c:539
msgid "Connecting…"
msgstr "Connexion…"

#: src/goabackend/goaexchangeprovider.c:537
#: src/goabackend/goaexchangeprovider.c:731
#: src/goabackend/goafedoraprovider.c:746
#: src/goabackend/goaimapsmtpprovider.c:865
#: src/goabackend/goaimapsmtpprovider.c:900
#: src/goabackend/goaimapsmtpprovider.c:992
#: src/goabackend/goaimapsmtpprovider.c:1249
#: src/goabackend/goaimapsmtpprovider.c:1325
#: src/goabackend/goakerberosprovider.c:939
#: src/goabackend/goalastfmprovider.c:644
#: src/goabackend/goalastfmprovider.c:812
#: src/goabackend/goamediaserverprovider.c:412
#: src/goabackend/goaoauth2provider.c:995 src/goabackend/goaoauthprovider.c:859
#: src/goabackend/goaowncloudprovider.c:661
#: src/goabackend/goaowncloudprovider.c:881
msgid "Dialog was dismissed"
msgstr "Lo dialòg es estat regetat"

#: src/goabackend/goaexchangeprovider.c:575
#: src/goabackend/goaexchangeprovider.c:759
#: src/goabackend/goafedoraprovider.c:792
#: src/goabackend/goaimapsmtpprovider.c:933
#: src/goabackend/goaimapsmtpprovider.c:1030
#: src/goabackend/goaimapsmtpprovider.c:1276
#: src/goabackend/goaimapsmtpprovider.c:1353
#: src/goabackend/goalastfmprovider.c:678
#: src/goabackend/goalastfmprovider.c:831
#: src/goabackend/goaowncloudprovider.c:706
#: src/goabackend/goaowncloudprovider.c:908
#, c-format
msgid "Dialog was dismissed (%s, %d): "
msgstr "Lo dialòg es estat regetat (%s, %d) : "

#: src/goabackend/goaexchangeprovider.c:588
#: src/goabackend/goaimapsmtpprovider.c:946
#: src/goabackend/goaimapsmtpprovider.c:1043
#: src/goabackend/goaowncloudprovider.c:719
msgid "_Ignore"
msgstr "_Ignorar"

#: src/goabackend/goaexchangeprovider.c:593
#: src/goabackend/goaexchangeprovider.c:778
#: src/goabackend/goafedoraprovider.c:803
#: src/goabackend/goaimapsmtpprovider.c:951
#: src/goabackend/goaimapsmtpprovider.c:1048
#: src/goabackend/goaimapsmtpprovider.c:1295
#: src/goabackend/goaimapsmtpprovider.c:1372
#: src/goabackend/goakerberosprovider.c:1032
#: src/goabackend/goalastfmprovider.c:690
#: src/goabackend/goalastfmprovider.c:848
#: src/goabackend/goaowncloudprovider.c:724
#: src/goabackend/goaowncloudprovider.c:927
msgid "_Try Again"
msgstr "_Ensajatz tornamai"

#: src/goabackend/goaexchangeprovider.c:598
#: src/goabackend/goaexchangeprovider.c:771
msgid "Error connecting to Microsoft Exchange server"
msgstr "Error de connexion al servidor Microsoft Exchange"

#: src/goabackend/goafacebookprovider.c:56
msgid "Facebook"
msgstr "Facebook"

#: src/goabackend/goafacebookprovider.c:183
#: src/goabackend/goaflickrprovider.c:155
#: src/goabackend/goafoursquareprovider.c:154
#: src/goabackend/goagoogleprovider.c:184
#: src/goabackend/goawindowsliveprovider.c:159
#, c-format
msgid ""
"Expected status 200 when requesting your identity, instead got status %d (%s)"
msgstr ""
"Estat 200 esperat al moment de la requèsta de vòstra identitat, estat %d "
"(%s) recebut a la plaça"

#. TODO: more specific
#: src/goabackend/goafacebookprovider.c:202
#: src/goabackend/goafacebookprovider.c:213
#: src/goabackend/goafacebookprovider.c:219
#: src/goabackend/goaflickrprovider.c:174
#: src/goabackend/goaflickrprovider.c:185
#: src/goabackend/goaflickrprovider.c:196
#: src/goabackend/goaflickrprovider.c:205
#: src/goabackend/goaflickrprovider.c:218
#: src/goabackend/goafoursquareprovider.c:173
#: src/goabackend/goafoursquareprovider.c:184
#: src/goabackend/goafoursquareprovider.c:195
#: src/goabackend/goafoursquareprovider.c:206
#: src/goabackend/goafoursquareprovider.c:215
#: src/goabackend/goafoursquareprovider.c:228
#: src/goabackend/goagoogleprovider.c:203
#: src/goabackend/goagoogleprovider.c:214
#: src/goabackend/goalastfmprovider.c:204
#: src/goabackend/goalastfmprovider.c:213
#: src/goabackend/goalastfmprovider.c:222
#: src/goabackend/goalastfmprovider.c:230
#: src/goabackend/goalastfmprovider.c:236
#: src/goabackend/goalastfmprovider.c:504
#: src/goabackend/goalastfmprovider.c:513
#: src/goabackend/goalastfmprovider.c:530
#: src/goabackend/goalastfmprovider.c:536
#: src/goabackend/goaoauth2provider.c:656
#: src/goabackend/goaoauth2provider.c:686
#: src/goabackend/goaoauth2provider.c:697 src/goabackend/goautils.c:296
#: src/goabackend/goawindowsliveprovider.c:178
#: src/goabackend/goawindowsliveprovider.c:189
#: src/goabackend/goawindowsliveprovider.c:198
#: src/goabackend/goawindowsliveprovider.c:211
msgid "Could not parse response"
msgstr "Impossible d'analisar la responsa"

#: src/goabackend/goafedoraprovider.c:94
msgid "Fedora"
msgstr "Fedora"

#: src/goabackend/goafedoraprovider.c:208
#: src/goabackend/goakerberosprovider.c:240
msgid "Ticketing is disabled for account"
msgstr "La proprietat Ticketing es definida per aqueste compte"

#: src/goabackend/goafedoraprovider.c:233
#: src/goabackend/goakerberosprovider.c:265
#, c-format
msgid "Could not find saved credentials for principal “%s” in keyring"
msgstr ""
"Impossible de trobar las donadas d’autentificacion del principal « %s » "
"enregistradas dins lo trossèl de claus"

#: src/goabackend/goafedoraprovider.c:246
#: src/goabackend/goakerberosprovider.c:278
#, c-format
msgid "Did not find password for principal “%s” in credentials"
msgstr ""
"Impossible de trobar lo senhal del principal « %s » dins las donadas "
"d’autentificacion"

#: src/goabackend/goafedoraprovider.c:805
msgid "Error connecting to Fedora"
msgstr " \tError de connexion a Fedora"

#: src/goabackend/goafedoraprovider.c:1198
#: src/goabackend/goakerberosprovider.c:1366
msgid "Identity service returned invalid key"
msgstr "Lo servici d'autentificacion a renviat « clau invalida »"

#: src/goabackend/goaflickrprovider.c:56
msgid "Flickr"
msgstr "Flickr"

#: src/goabackend/goaflickrprovider.c:271
msgid "Your system time is invalid. Check your date and time settings."
msgstr ""
"La data de vòstre sistèma es pas valida. Verificatz vòstres paramètres de "
"data e ora."

#: src/goabackend/goafoursquareprovider.c:57
msgid "Foursquare"
msgstr "Foursquare"

#: src/goabackend/goagoogleprovider.c:56
msgid "Google"
msgstr "Google"

#. TODO: more specific
#: src/goabackend/goaimapauthlogin.c:84 src/goabackend/goasmtpauth.c:149
msgid "Service not available"
msgstr "Servici indisponible"

#. TODO: more specific
#: src/goabackend/goaimapauthlogin.c:104 src/goabackend/goalastfmprovider.c:521
#: src/goabackend/goasmtpauth.c:102 src/goabackend/goautils.c:861
msgid "Authentication failed"
msgstr "Fracàs d'autentificacion"

#: src/goabackend/goaimapauthlogin.c:129
msgid "Server does not support PLAIN"
msgstr "Lo servidor pren pas en carga PLAIN"

#: src/goabackend/goaimapauthlogin.c:181 src/goabackend/goasmtpauth.c:600
msgid "Server does not support STARTTLS"
msgstr "Lo servidor pren pas en carga STARTTLS"

#: src/goabackend/goaimapsmtpprovider.c:53
msgid "IMAP and SMTP"
msgstr "IMAP e SMTP"

#. Translators: the first %s is a field name. The
#. * second %s is the IMAP
#. * username (eg., rishi), and the (%s, %d)
#. * is the error domain and code.
#.
#. Translators: the first %s is a field name. The
#. * second %s is the SMTP
#. * username (eg., rishi), and the (%s, %d)
#. * is the error domain and code.
#.
#: src/goabackend/goaimapsmtpprovider.c:327
#: src/goabackend/goaimapsmtpprovider.c:378
#, c-format
msgid "Invalid %s with username “%s” (%s, %d): "
msgstr "%s invalid per l’utilizaire « %s » (%s, %d) : "

#. Translators: the following four strings are used to show a
#. * combo box similar to the one in the evolution module.
#. * Encryption: None
#. *             STARTTLS after connecting
#. *             SSL on a dedicated port
#.
#: src/goabackend/goaimapsmtpprovider.c:564
msgid "_Encryption"
msgstr "_Chiframent"

#: src/goabackend/goaimapsmtpprovider.c:567
msgid "None"
msgstr "Pas cap"

#: src/goabackend/goaimapsmtpprovider.c:570
msgid "STARTTLS after connecting"
msgstr "STARTTLS aprèp la connexion"

#: src/goabackend/goaimapsmtpprovider.c:573
msgid "SSL on a dedicated port"
msgstr "SSL sus un pòrt dedicat"

#: src/goabackend/goaimapsmtpprovider.c:648
msgid "_Name"
msgstr "_Nom"

#: src/goabackend/goaimapsmtpprovider.c:665
msgid "IMAP _Server"
msgstr "_Servidor IMAP"

#: src/goabackend/goaimapsmtpprovider.c:685
msgid "SMTP _Server"
msgstr "_Servidor SMTP"

#: src/goabackend/goaimapsmtpprovider.c:700
#: src/goabackend/goaimapsmtpprovider.c:972
#: src/goabackend/goaimapsmtpprovider.c:1305
msgid "_Forward"
msgstr "_Seguent"

#: src/goabackend/goaimapsmtpprovider.c:956
#: src/goabackend/goaimapsmtpprovider.c:1288
msgid "Error connecting to IMAP server"
msgstr "Error de connexion al servidor IMAP"

#: src/goabackend/goaimapsmtpprovider.c:1053
#: src/goabackend/goaimapsmtpprovider.c:1365
msgid "Error connecting to SMTP server"
msgstr "Error de connexion al servidor SMTP"

#: src/goabackend/goaimapsmtpprovider.c:1476
msgid "E-mail"
msgstr "Corrièr electronic"

#: src/goabackend/goaimapsmtpprovider.c:1480
msgid "Name"
msgstr "Nom"

#: src/goabackend/goaimapsmtpprovider.c:1490
#: src/goabackend/goaimapsmtpprovider.c:1494
msgid "IMAP"
msgstr "IMAP"

#: src/goabackend/goaimapsmtpprovider.c:1505
#: src/goabackend/goaimapsmtpprovider.c:1509
msgid "SMTP"
msgstr "SMTP"

#: src/goabackend/goakerberosprovider.c:92
msgid "Enterprise Login (Kerberos)"
msgstr "Connexion d'entrepresa (Kerberos)"

#: src/goabackend/goakerberosprovider.c:578
msgid "_Principal"
msgstr "_Principal"

#: src/goabackend/goakerberosprovider.c:793
msgid "Operation was cancelled"
msgstr "L’operacion es estada anullada"

#: src/goabackend/goakerberosprovider.c:837
#: src/goaidentity/goaidentityservice.c:1265
msgid "Log In to Realm"
msgstr "Connexion al nom de domeni"

#: src/goabackend/goakerberosprovider.c:838
msgid "Please enter your password below."
msgstr "Picatz vòstre senhal çaijós."

#: src/goabackend/goakerberosprovider.c:839
msgid "Remember this password"
msgstr "Se remembrar d'aqueste senhal"

#: src/goabackend/goakerberosprovider.c:1027
msgid "Error connecting to enterprise identity server"
msgstr "Error de connexion al servidor d'identitat de l'entrepresa"

#: src/goabackend/goalastfmprovider.c:57
msgid "Last.fm"
msgstr "Last.fm"

#: src/goabackend/goalastfmprovider.c:692
#: src/goabackend/goalastfmprovider.c:842
msgid "Error connecting to Last.fm"
msgstr "Error de connexion a Last.fm"

#: src/goabackend/goamediaserverprovider.c:55
msgid "Media Server"
msgstr "Servidor multimèdia"

#: src/goabackend/goamediaserverprovider.c:308
msgid ""
"Personal content can be added to your applications through a media server "
"account."
msgstr ""
"De donadas personalas pòdon èsser apondudas a vòstra aplicacion via un "
"compte de servidor multimèdia."

#: src/goabackend/goamediaserverprovider.c:321
msgid "Available Media Servers"
msgstr "Servidor multimèdias disponibles"

#: src/goabackend/goamediaserverprovider.c:352
msgid "No media servers found"
msgstr "Cap de servidor multimèdia trobat"

#. Translators: the %d is a HTTP status code and the %s is a textual description of it
#: src/goabackend/goaoauth2provider.c:632 src/goabackend/goaoauthprovider.c:542
#, c-format
msgid ""
"Expected status 200 when requesting access token, instead got status %d (%s)"
msgstr ""
"Estat 200 esperat al moment de la requèsta del geton d'accès, estat %d (%s) "
"recebut a la plaça"

#: src/goabackend/goaoauth2provider.c:814
msgid "Authorization response: "
msgstr "Responsa d'autorizacion : "

#: src/goabackend/goaoauth2provider.c:884
#, c-format
msgid "Authorization response: %s"
msgstr "Responsa d'autorization : %s"

#: src/goabackend/goaoauth2provider.c:1021
#: src/goabackend/goaoauthprovider.c:890
msgid "Error getting an Access Token: "
msgstr "Error al moment de l'obtencion del geton d'accès : "

#: src/goabackend/goaoauth2provider.c:1036
#: src/goabackend/goaoauthprovider.c:903
msgid "Error getting identity: "
msgstr "Error al moment de l'obtencion d'una identitat : "

#: src/goabackend/goaoauth2provider.c:1230
#: src/goabackend/goaoauthprovider.c:1168
#, c-format
msgid "Was asked to log in as %s, but logged in as %s"
msgstr ""
"Èra demandat de se connectar en tant que %s, mas la connexion es estada "
"establida en tant que %s"

#: src/goabackend/goaoauth2provider.c:1382
msgid "Credentials do not contain access_token"
msgstr "Las donadas d'autentificacion contenon pas access_token"

#: src/goabackend/goaoauth2provider.c:1421
#: src/goabackend/goaoauthprovider.c:1383
#, c-format
msgid "Failed to refresh access token (%s, %d): "
msgstr "Fracàs de l'actualizacion del geton d'accès (%s, %d) : "

#: src/goabackend/goaoauthprovider.c:565
msgid "Missing access_token or access_token_secret headers in response"
msgstr "Entèstas access_token o access_token_secret mancants dins la responsa"

#: src/goabackend/goaoauthprovider.c:773
msgid "Error getting a Request Token: "
msgstr "Error d'obtencion d'un geton de requèsta : "

#. Translators: the %d is a HTTP status code and the %s is a textual description of it
#: src/goabackend/goaoauthprovider.c:804
#, c-format
msgid ""
"Expected status 200 for getting a Request Token, instead got status %d (%s)"
msgstr ""
"Estat 200 esperat al moment de l'obtencion d'un geton de requèsta, estat %d "
"(%s) recebut a la plaça"

#: src/goabackend/goaoauthprovider.c:821
msgid "Missing request_token or request_token_secret headers in response"
msgstr ""
"Entèstas request_token o request_token_secret mancantas dins la responsa"

#: src/goabackend/goaoauthprovider.c:1339
msgid "Credentials do not contain access_token or access_token_secret"
msgstr ""
"Las donadas d'autentificacion contenon pas access_token o access_token_secret"

#: src/goabackend/goaowncloudprovider.c:60
msgid "Nextcloud"
msgstr "Nextcloud"

#: src/goabackend/goaowncloudprovider.c:729
#: src/goabackend/goaowncloudprovider.c:920
msgid "Error connecting to Nextcloud server"
msgstr "Error de connexion al servidor Nextcloud"

#: src/goabackend/goaprovider.c:112
msgid "_Mail"
msgstr "Co_rrièr electronic"

#: src/goabackend/goaprovider.c:117
msgid "Cale_ndar"
msgstr "Age_nda"

#: src/goabackend/goaprovider.c:122
msgid "_Contacts"
msgstr "_Contactes"

#: src/goabackend/goaprovider.c:127
msgid "C_hat"
msgstr "D_iscussion"

#: src/goabackend/goaprovider.c:132
msgid "_Documents"
msgstr "_Documents"

#: src/goabackend/goaprovider.c:137
msgid "M_usic"
msgstr "M_usica"

#: src/goabackend/goaprovider.c:142
msgid "_Photos"
msgstr "_Fòtos"

#: src/goabackend/goaprovider.c:147
msgid "_Files"
msgstr "_Fichièrs"

#: src/goabackend/goaprovider.c:152
msgid "Network _Resources"
msgstr "_Ressorsas de la ret"

#: src/goabackend/goaprovider.c:157
msgid "_Read Later"
msgstr "_Legir pus tard"

#: src/goabackend/goaprovider.c:162
msgid "Prin_ters"
msgstr "Imprimen_tas"

#: src/goabackend/goaprovider.c:167
msgid "_Maps"
msgstr "_Mapas"

#: src/goabackend/goaprovider.c:172
msgid "T_o Do"
msgstr "Prèt_zfaits"

#. Translators: This is a label for a series of
#. * options switches. For example: “Use for Mail”.
#: src/goabackend/goaprovider.c:570
msgid "Use for"
msgstr "Utilizar per"

#: src/goabackend/goaprovider.c:825
msgid "Account is disabled"
msgstr "Lo compte es desactivat"

#: src/goabackend/goaprovider.c:838
msgid "Unknown error"
msgstr "Error desconeguda"

#: src/goabackend/goaprovider.c:857
#, c-format
msgid "ensure_credentials_sync is not implemented on type %s"
msgstr "ensure_credentials_sync es pas implementat pel tipe %s"

#. TODO: more specific
#: src/goabackend/goasmtpauth.c:164
msgid "TLS not available"
msgstr "TLS indisponible"

#: src/goabackend/goasmtpauth.c:470
msgid "Unknown authentication mechanism"
msgstr "Metòde d'autentificacion desconegut"

#: src/goabackend/goautils.c:92
msgid "Error logging into the account"
msgstr "Error a la connexion al compte"

#: src/goabackend/goautils.c:136
msgid "Credentials have expired"
msgstr "Las donadas d’autentificacion an expirat"

#: src/goabackend/goautils.c:140
msgid "Sign in to enable this account."
msgstr "Identificatz-vos per activar aqueste compte"

#: src/goabackend/goautils.c:144
msgid "_Sign In"
msgstr "_S’identificar"

#: src/goabackend/goautils.c:240
#, c-format
msgid "A %s account already exists for %s"
msgstr "Un compte %s existís ja per %s"

#. Translators: this is the title of the "Add Account" and "Refresh
#. * Account" dialogs. The %s is the name of the provider. eg.,
#. * 'Google'.
#.
#: src/goabackend/goautils.c:316
#, c-format
msgid "%s Account"
msgstr "Compte %s"

#. TODO: more specific
#: src/goabackend/goautils.c:370
msgid "Failed to delete credentials from the keyring"
msgstr ""
"Fracàs de supression de las donadas d'autentificacion del trossèl de claus"

#. TODO: more specific
#: src/goabackend/goautils.c:418
msgid "Failed to retrieve credentials from the keyring"
msgstr ""
"Fracàs al moment de la recuperacion de las donadas d'autentificacion dempuèi "
"lo trossèl de claus"

#. TODO: more specific
#: src/goabackend/goautils.c:428
msgid "No credentials found in the keyring"
msgstr "Cap de donada d'autentificacion pas trobada dins lo trossèl de claus"

#: src/goabackend/goautils.c:441
msgid "Error parsing result obtained from the keyring: "
msgstr ""
"Error al moment de l'analisi del resultat obtengut a partir del trossèl de "
"claus : "

#. Translators: The %s is the type of the provider, e.g. 'google' or 'yahoo'
#: src/goabackend/goautils.c:482
#, c-format
msgid "GOA %s credentials for identity %s"
msgstr "Donadas d'autentificacion GOA %s per l'identitat %s"

#. TODO: more specific
#: src/goabackend/goautils.c:499
msgid "Failed to store credentials in the keyring"
msgstr ""
"Fracàs al moment de l'emmagazinatge de las donadas d'autentificacion dins lo "
"trossèl de claus"

#: src/goabackend/goautils.c:848
msgid "Cannot resolve hostname"
msgstr "Impossible de resòlvre lo nom de domeni"

#: src/goabackend/goautils.c:852
msgid "Cannot resolve proxy hostname"
msgstr "Impossible de resòlvre lo nom de domeni del servidor mandatari"

#: src/goabackend/goautils.c:857
msgid "Cannot find WebDAV endpoint"
msgstr "Impossible de trobar lo punt de sortida WebDAV"

#: src/goabackend/goautils.c:866
#, c-format
msgid "Code: %u — Unexpected response from server"
msgstr "Còdi : %u — Responsa inesperada del servidor"

#: src/goabackend/goautils.c:882
msgid "The signing certificate authority is not known."
msgstr "L'autoritat de certificat de signatura es desconeguda."

#: src/goabackend/goautils.c:886
msgid ""
"The certificate does not match the expected identity of the site that it was "
"retrieved from."
msgstr ""
"Lo certificat correspond pas a l'identitat esperada pel site d'ont proven."

#: src/goabackend/goautils.c:891
msgid "The certificate’s activation time is still in the future."
msgstr "La data d'activacion del certificat es totjorn dins lo futur."

#: src/goabackend/goautils.c:895
msgid "The certificate has expired."
msgstr "Lo certificat a expirat."

#: src/goabackend/goautils.c:899
msgid "The certificate has been revoked."
msgstr "Lo certificat es estat revocat."

#: src/goabackend/goautils.c:903
msgid "The certificate’s algorithm is considered insecure."
msgstr "L'algoritme del certificat es pas considerat coma segur."

#: src/goabackend/goautils.c:909
msgid "Invalid certificate."
msgstr "Certificat invalid."

#. TODO: more specific
#: src/goabackend/goautils.c:944
#, c-format
msgid "Did not find %s with identity “%s” in credentials"
msgstr ""
"I a pas cap de %s amb l’identitat « %s » dins las donadas d’autentificacion"

#. translators: %s here is the address of the web page
#: src/goabackend/goawebview.c:89
#, c-format
msgid "Loading “%s”…"
msgstr "Cargament de « %s »…"

#: src/goabackend/goawindowsliveprovider.c:56
msgid "Microsoft"
msgstr "Microsoft"

#: src/goaidentity/goaidentityservice.c:564
msgid "Initial secret key is invalid"
msgstr "La clau secreta iniciala es pas valida"

#. TODO: more specific
#: src/goaidentity/goaidentityservice.c:786
#: src/goaidentity/goaidentityservice.c:890
#, c-format
msgid "Couldn't get GoaObject for object path %s"
msgstr "Impossible d’obténer GoaObject pel camin d’objècte %s"

#: src/goaidentity/goaidentityservice.c:1270
#, c-format
msgid "The network realm %s needs some information to sign you in."
msgstr ""
"Lo nom de domeni de la ret %s a besonh de qualques informacions per vos "
"connectar."

#: src/goaidentity/goakerberosidentity.c:271
#: src/goaidentity/goakerberosidentity.c:279
#: src/goaidentity/goakerberosidentity.c:666
msgid "Could not find identity in credential cache: "
msgstr ""
"Impossible de trobar l’identitat dins lo cache de la donada "
"d’autentificacion : "

#: src/goaidentity/goakerberosidentity.c:678
msgid "Could not find identity credentials in cache: "
msgstr ""
"Impossible de trobar las donadas d’autentificacion de l’identitat dins lo "
"cache : "

#: src/goaidentity/goakerberosidentity.c:721
msgid "Could not sift through identity credentials in cache: "
msgstr ""
"Impossible de passar pel crivèl las donadas d’autentificacion de l’identitat "
"dins lo cache : "

#: src/goaidentity/goakerberosidentity.c:735
msgid "Could not finish up sifting through identity credentials in cache: "
msgstr ""
"Impossible d’acabar lo passatge pel crivèl de las donadas d’autentificacion "
"de l’identitat dins lo cache : "

#: src/goaidentity/goakerberosidentity.c:1019
msgid "No associated identification found"
msgstr "Cap d'autentificacion associada pas trobada"

#: src/goaidentity/goakerberosidentity.c:1100
msgid "Could not create credential cache: "
msgstr "Impossible de crear lo cache de las donadas d’autentificacion : "

#: src/goaidentity/goakerberosidentity.c:1132
msgid "Could not initialize credentials cache: "
msgstr "Impossible d’inicializar lo cache de las donadas d’autentificacion : "

#: src/goaidentity/goakerberosidentity.c:1145
msgid "Could not store new credentials in credentials cache: "
msgstr ""
"Impossible d'emmagazinar de novèlas donadas d’autentificacion dins lo cache "
"de las donadas d’autentificacion : "

#: src/goaidentity/goakerberosidentity.c:1433
msgid "Could not renew identity: Not signed in"
msgstr "Impossible de renovelar l'identitat : sètz pas connectat"

#: src/goaidentity/goakerberosidentity.c:1443
msgid "Could not renew identity: "
msgstr "Impossible de renovelar l’identitat : "

#: src/goaidentity/goakerberosidentity.c:1456
#, c-format
msgid "Could not get new credentials to renew identity %s: "
msgstr ""
"Impossible d’obténer de novèlas donadas d’autentificacion per renovelar "
"l’identitat %s : "

#: src/goaidentity/goakerberosidentity.c:1497
msgid "Could not erase identity: "
msgstr "Impossible d’escafar l’identitat : "

#: src/goaidentity/goakerberosidentitymanager.c:816
msgid "Could not find identity"
msgstr "Impossible de trobar l'identitat"

#: src/goaidentity/goakerberosidentitymanager.c:897
msgid "Could not create credential cache for identity"
msgstr ""
"Impossible de crear lo cache de la donada d'autentificacion per l'identitat"

#~ msgid "Pocket"
#~ msgstr "Pocket"

#~ msgid "No username or access_token"
#~ msgstr "Pas de nom d'utilizaire o de geton d'accès"

#~ msgid "Todoist"
#~ msgstr "Todoist"

#~ msgid "_Domain"
#~ msgstr "_Domeni"

#~ msgid "Enterprise domain or realm name"
#~ msgstr "Domeni d'entrepresa o nom de domeni"

#~ msgid "The domain is not valid"
#~ msgstr "Lo domeni es pas valid"

#~ msgid "ownCloud"
#~ msgstr "ownCloud"

#~ msgid "org.gnome.OnlineAccounts.Mail is not available"
#~ msgstr "org.gnome.OnlineAccounts.Mail es pas disponible"

#~ msgid "Failed to parse email address"
#~ msgstr "Fracàs de l'analisi de l'adreça corrièr electronic"

#~ msgid "Cannot do SMTP authentication without a domain"
#~ msgstr "Impossible de far una autentificacion SMTP sens domeni"

#~ msgid "Did not find smtp-password in credentials"
#~ msgstr ""
#~ "Impossible de trobar un senhal SMTP dins las donadas d'autentificacion"

#~ msgid "Cannot do SMTP authentication without a password"
#~ msgstr "Impossible de far una autentificacion SMTP sens senhal"

#~ msgid "Telepathy chat account not found"
#~ msgstr "Cap de compte de discussion Telepathy pas trobat"

#~ msgid "Failed to initialize a GOA client"
#~ msgstr "Impossible d'inicializar un compte en linha"

#~ msgid "Failed to create a user interface for %s"
#~ msgstr "Impossible de crear una interfàcia utilizaire per %s"

#~ msgid "Connection Settings"
#~ msgstr "Paramètres de connexion"

#~ msgid "Personal Details"
#~ msgstr "Informacions personalas"

#~ msgid "_OK"
#~ msgstr "_Validar"

#~ msgid "Cannot save the connection parameters"
#~ msgstr "Impossible d'enregistrar los paramètres de connexion"

#~ msgid "Cannot save your personal information on the server"
#~ msgstr ""
#~ "Impossible d'enregistrar vòstras informacions personalas sul servidor"

#~ msgid "_Connection Settings"
#~ msgstr "_Paramètres de connexion"

#~ msgid "_Personal Details"
#~ msgstr "_Informacions personalas"

#~ msgid "initial secret passed before secret key exchange"
#~ msgstr "secret inicial provesit abans l'escambi de la clau secreta"

#~ msgid "Code: %u - Unexpected response from server"
#~ msgstr "Còdi : %u - Responsa inesperada del servidor"

#~ msgid "Failed to find Autodiscover element"
#~ msgstr "Impossible de trobar l'element Autodiscover"

#~ msgid "Failed to find Response element"
#~ msgstr "Impossible de trobar l'element Response"

#~ msgid "Failed to find Account element"
#~ msgstr "Impossible de trobar l'element Account"

#~ msgid "Did not find password with identity `%s' in credentials"
#~ msgstr ""
#~ "I a pas cap de senhal amb l'identitat « %s » dins las donadas "
#~ "d'autentificacion"

#~ msgid "Invalid password with username `%s' (%s, %d): "
#~ msgstr "Senhal invalid per l'utilizaire « %s » (%s, %d) : "

#~ msgid "Expected status 200 when requesting guid, instead got status %d (%s)"
#~ msgstr ""
#~ "Estat 200 esperat al moment de la requèsta del guid, estat %d (%s) "
#~ "recebut a la plaça"

#~ msgid ""
#~ "Expected status 200 when requesting user id, instead got status %d (%s)"
#~ msgstr ""
#~ "Estat 200 esperat al moment de la requèsta de l'identificant utilizaire, "
#~ "estat %d (%s) recebut a la plaça"

#~ msgid "Did not find imap-password with identity `%s' in credentials"
#~ msgstr ""
#~ "I a pas cap de senhal IMAP amb l'identitat « %s » dins las donadas "
#~ "d'autentificacion"

#~ msgid "Invalid imap-password with username `%s' (%s, %d): "
#~ msgstr "Senhal IMAP invalid per l'utilizaire « %s » (%s, %d) : "

#~ msgid "Did not find smtp-password with identity `%s' in credentials"
#~ msgstr ""
#~ "I a pas cap de senhal SMTP amb l'identitat « %s » dins las donadas "
#~ "d'autentificacion"

#~ msgid "Invalid smtp-password with username `%s' (%s, %d): "
#~ msgstr "Senhal SMTP invalid per l'utilizaire « %s » (%s, %d) : "

#~ msgid "Could not find saved credentials for principal `%s' in keyring"
#~ msgstr ""
#~ "Impossible de trobar las donadas d'autentificacion del principal « %s » "
#~ "enregistradas dins lo trossèl de claus"

#~ msgid "Did not find password for principal `%s' in credentials"
#~ msgstr ""
#~ "Impossible de trobar lo senhal del principal « %s » dins las donadas "
#~ "d'autentificacion"

#~ msgid "Authorization response was \"%s\""
#~ msgstr "La responsa d'autorizacion èra « %s »"

#~ msgid ""
#~ "Paste authorization code obtained from the <a href=\"%s\">authorization "
#~ "page</a>:"
#~ msgstr ""
#~ "Pegar lo còdi d'autorizacion obtengut dempuèi la <a href=\"%s\">pagina "
#~ "d'autorizacion</a> :"

#~ msgid "Was asked to login as %s, but logged in as %s"
#~ msgstr ""
#~ "Èra demandat de se connectar en tant que %s, mas la connexion es estada "
#~ "establida en tant que %s"

#~ msgid "Paste token obtained from the <a href=\"%s\">authorization page</a>:"
#~ msgstr ""
#~ "Pegar lo geton obtengut dempuèi la <a href=\"%s\">pagina d'autorizacion</"
#~ "a> :"

#~ msgid "Cannot do SMTP PLAIN without a domain"
#~ msgstr "Impossible de far de SMTP PLAIN sens domeni"

#~ msgid "Cannot do SMTP PLAIN without a password"
#~ msgstr "Impossible de far de SMTP PLAIN sens senhal"

#~ msgid "Twitter"
#~ msgstr "Twitter"

#~ msgid "The certificate's activation time is still in the future."
#~ msgstr "La data d'activacion del certificat es totjorn dins l'avenidor."

#~ msgid "The certificate's algorithm is considered insecure."
#~ msgstr "L'algoritme del certificat es pas considerat coma segur."

#~ msgid "Windows Live"
#~ msgstr "Windows Live"

#~ msgid "Yahoo"
#~ msgstr "Yahoo"

#~ msgid "Expected status 200 when requesting name, instead got status %d (%s)"
#~ msgstr ""
#~ "Estat 200 esperat al moment de la requèsta del nom, estat %d (%s) recebut "
#~ "a la plaça"

#~ msgid "Time"
#~ msgstr "Temps"

#~ msgid "Time to fire"
#~ msgstr "Temps d'activacion"

#~ msgid "Online Accounts"
#~ msgstr "Comptes en linha"

#~ msgid "An online account needs atencion"
#~ msgstr "Un compte en linha requerís vòstra atencion"

#~ msgid "Open Online Accounts..."
#~ msgstr "Dobrir los comptes en linha..."

#~ msgid "Credentials not found in keyring (%s, %d): "
#~ msgstr ""
#~ "Donadas d'autentificacion introuvables dins lo trossèl de claus (%s, "
#~ "%d) : "

#~ msgid "Error parsing response as JSON: "
#~ msgstr "Error d'analyse de la responsa al format JSON : "

#~ msgid "Didn't find id member in JSON data"
#~ msgstr "Impossible de trobar lo membre id dins las donadas JSON"

#~ msgid "Didn't find email member in JSON data"
#~ msgstr "Impossible de trobar lo membre email dins las donadas JSON"

#~ msgid "Didn't find data member in JSON data"
#~ msgstr "Impossible de trobar lo membre data dins las donadas JSON"

#~ msgid "Didn't find access_token in non-JSON data"
#~ msgstr "Impossible de trobar access_token dins las donadas non-JSON"

#~ msgid "Didn't find access_token in JSON data"
#~ msgstr "Impossible de trobar access_token dins las donadas JSON"

#~ msgid "Error storing credentials in keyring (%s, %d): "
#~ msgstr ""
#~ "Error al moment de l'emmagazinatge de las donadas d'autentificacion dins "
#~ "lo trossèl de claus (%s, %d) : "

#~ msgid "Didn't find account email member in JSON data"
#~ msgstr "Impossible de trobar lo membre account email dins las donadas JSON"

#~ msgid "Domain Administrator Login"
#~ msgstr "Identificacion de l'administrator del domeni"

#~ msgid ""
#~ "In order to use this enterprise identity, the computer needs to be "
#~ "enrolled in the domain. Please have your network administrator type their "
#~ "domain password here."
#~ msgstr ""
#~ "Per poder utilizar aquesta identitat d'entrepresa, aqueste ordenador deu "
#~ "èsser inscrit al domeni. Vòstre administrator ret deu sasir son senhal de "
#~ "domeni aicí."

#~ msgid ""
#~ "In order to use this enterprise identity, the computer needs to be "
#~ "enrolled in the domain. Please have your network administrator type their "
#~ "domain username here."
#~ msgstr ""
#~ "Per poder utilizar aquesta identitat d'entrepresa, aqueste ordenador deu "
#~ "èsser inscrit al domeni. Vòstre administrator ret deu sasir son nom "
#~ "d'utilizaire de domeni aicí."

#~ msgid "No such domain or realm found"
#~ msgstr "Cap de domeni o nom de domeni correspondent pas trobat"

#~ msgid "Cannot log in as %s at the %s domain"
#~ msgstr "Impossible de se connectar en tant que %s al domeni %s"

#~ msgid "Invalid password, please try again"
#~ msgstr "Senhal invalid, ensajatz tornamai"

#~ msgid "Couldn't connect to the %s domain: %s"
#~ msgstr "Impossible de se connectar al domeni %s : %s"

#~ msgid "Chat"
#~ msgstr "Discussion"

#~ msgid "New Microsoft Exchange Account"
#~ msgstr "Novèl compte Microsoft Exchange"

#~ msgid "Microsoft Exchange Account"
#~ msgstr "Compte Microsoft Exchange"

#~ msgid "New Enterprise Login (Kerberos)"
#~ msgstr "Novèla connexion d'entrepresa (Kerberos)"

#~ msgid "Add %s"
#~ msgstr "Apondre %s"

#~ msgid "Refresh %s"
#~ msgstr "Actualizar %s"

#~ msgid "Did not find password with identity ‘%s’ in credentials"
#~ msgstr ""
#~ "I a pas cap de senhal amb l'identitat « %s » dins las donadas "
#~ "d'autentificacion"
